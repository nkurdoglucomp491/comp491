package game;

import java.util.Vector;

public class CheckersDataBritish extends CheckersData{

	public CheckersMoveBritish[] legalMoves;
	
	public static final int
	EMPTY = 0,
	RED = -1,
	RED_KING = -2,
	BLACK = 1,
	BLACK_KING = 2;

	public CheckersDataBritish() {
		// Constructor.  Create the board and set it up for a new game.
		super();
		setUpGame();
	}
	
	public void setUpGame() {
		for (int row = 0; row < 8; row++) {
			for (int col = 0; col < 8; col++) {
				if ( row % 2 == col % 2 ) {
					if (row < 3)
						board[row][col] = BLACK;
					else if (row > 4)
						board[row][col] = RED;
					else
						board[row][col] = EMPTY;
				}
				else {
					board[row][col] = EMPTY;
				}
			}
		}
	}  // end setUpGame()

	public void makeMove(CheckersMove move) {
		// Make the specified move.  It is assumed that move
		// is non-null and that the move it represents is legal.
		makeMove(move.fromRow, move.fromCol, move.toRow, move.toCol);
	}


	public void makeMove(int fromRow, int fromCol, int toRow, int toCol) {
		// Make the move from (fromRow,fromCol) to (toRow,toCol).  It is
		// assumed that this move is legal.  If the move is a jump, the
		// jumped piece is removed from the board.  If a piece moves
		// the last row on the opponent's side of the board, the 
		// piece becomes a king.
		board[toRow][toCol] = board[fromRow][fromCol];
		board[fromRow][fromCol] = EMPTY;
		if (fromRow - toRow == 2 || fromRow - toRow == -2) {
			// The move is a jump.  Remove the jumped piece from the board.
			int jumpRow = (fromRow + toRow) / 2;  // Row of the jumped piece.
			int jumpCol = (fromCol + toCol) / 2;  // Column of the jumped piece.
			board[jumpRow][jumpCol] = EMPTY;
		}
		if (toRow == 0 && board[toRow][toCol] == RED)
			board[toRow][toCol] = RED_KING;
		if (toRow == 7 && board[toRow][toCol] == BLACK)
			board[toRow][toCol] = BLACK_KING;
	}


	public CheckersMoveBritish[] getLegalMoves(int player) {

		if (player != RED && player != BLACK)
			return null;

		int playerKing;  // The constant representing a King belonging to player.
		if (player == RED)
			playerKing = RED_KING;
		else
			playerKing = BLACK_KING;

		Vector<CheckersMoveBritish> moves = new Vector<CheckersMoveBritish>();  // Moves will be stored in this vector.

		/*  First, check for any possible jumps.  Look at each square on the board.
          If that square contains one of the player's pieces, look at a possible
          jump in each of the four directions from that square.  If there is 
          a legal jump in that direction, put it in the moves vector.
		 */

		for (int row = 0; row < 8; row++) {
			for (int col = 0; col < 8; col++) {
				if (board[row][col] == player || board[row][col] == playerKing) {
					if (canJump(player, row, col, row+1, col+1, row+2, col+2))
						moves.addElement(new CheckersMoveBritish(row, col, row+2, col+2));
					if (canJump(player, row, col, row-1, col+1, row-2, col+2))
						moves.addElement(new CheckersMoveBritish(row, col, row-2, col+2));
					if (canJump(player, row, col, row+1, col-1, row+2, col-2))
						moves.addElement(new CheckersMoveBritish(row, col, row+2, col-2));
					if (canJump(player, row, col, row-1, col-1, row-2, col-2))
						moves.addElement(new CheckersMoveBritish(row, col, row-2, col-2));
				}
			}
		}

		/*  If any jump moves were found, then the user must jump, so we don't 
          add any regular moves.  However, if no jumps were found, check for
          any legal regualar moves.  Look at each square on the board.
          If that square contains one of the player's pieces, look at a possible
          move in each of the four directions from that square.  If there is 
          a legal move in that direction, put it in the moves vector.
		 */

		if (moves.size() == 0) {
			for (int row = 0; row < 8; row++) {
				for (int col = 0; col < 8; col++) {
					if (board[row][col] == player || board[row][col] == playerKing) {
						if (canMove(player,row,col,row+1,col+1))
							moves.addElement(new CheckersMoveBritish(row,col,row+1,col+1));
						if (canMove(player,row,col,row-1,col+1))
							moves.addElement(new CheckersMoveBritish(row,col,row-1,col+1));
						if (canMove(player,row,col,row+1,col-1))
							moves.addElement(new CheckersMoveBritish(row,col,row+1,col-1));
						if (canMove(player,row,col,row-1,col-1))
							moves.addElement(new CheckersMoveBritish(row,col,row-1,col-1));
					}
				}
			}
		}

		/* If no legal moves have been found, return null.  Otherwise, create
         an array just big enough to hold all the legal moves, copy the
         legal moves from the vector into the array, and return the array. */

		if (moves.size() == 0)
			return null;
		else {
			CheckersMoveBritish[] moveArray = new CheckersMoveBritish[moves.size()];
			for (int i = 0; i < moves.size(); i++)
				moveArray[i] = moves.elementAt(i);
			return moveArray;
		}

	}  // end getLegalMoves


	public CheckersMoveBritish[] getLegalJumpsFrom(int player, int row, int col) {

		if (player != RED && player != BLACK)
			return null;
		int playerKing;  // The constant representing a King belonging to player.
		if (player == RED)
			playerKing = RED_KING;
		else
			playerKing = BLACK_KING;
		Vector<CheckersMoveBritish> moves = new Vector<CheckersMoveBritish>();  // The legal jumps will be stored in this vector.
		if (board[row][col] == player || board[row][col] == playerKing) {
			if (canJump(player, row, col, row+1, col+1, row+2, col+2))
				moves.addElement(new CheckersMoveBritish(row, col, row+2, col+2));
			if (canJump(player, row, col, row-1, col+1, row-2, col+2))
				moves.addElement(new CheckersMoveBritish(row, col, row-2, col+2));
			if (canJump(player, row, col, row+1, col-1, row+2, col-2))
				moves.addElement(new CheckersMoveBritish(row, col, row+2, col-2));
			if (canJump(player, row, col, row-1, col-1, row-2, col-2))
				moves.addElement(new CheckersMoveBritish(row, col, row-2, col-2));
		}
		if (moves.size() == 0)
			return null;
		else {
			CheckersMoveBritish[] moveArray = new CheckersMoveBritish[moves.size()];
			for (int i = 0; i < moves.size(); i++)
				moveArray[i] = moves.elementAt(i);
			return moveArray;
		}
	}  // end getLegalMovesFrom()


	private boolean canJump(int player, int r1, int c1, int r2, int c2, int r3, int c3) {
		// This is called by the two previous methods to check whether the
		// player can legally jump from (r1,c1) to (r3,c3).  It is assumed
		// that the player has a piece at (r1,c1), that (r3,c3) is a position
		// that is 2 rows and 2 columns distant from (r1,c1) and that 
		// (r2,c2) is the square between (r1,c1) and (r3,c3).

		if (r3 < 0 || r3 >= 8 || c3 < 0 || c3 >= 8)
			return false;  // (r3,c3) is off the board.

		if (board[r3][c3] != EMPTY)
			return false;  // (r3,c3) already contains a piece.

		if (player == RED) {
			if (board[r1][c1] == RED && r3 > r1)
				return false;  // Regular red piece can only move  up.
			if (board[r2][c2] != BLACK && board[r2][c2] != BLACK_KING)
				return false;  // There is no black piece to jump.
			return true;  // The jump is legal.
		}
		else {
			if (board[r1][c1] == BLACK && r3 < r1)
				return false;  // Regular black piece can only move downn.
			if (board[r2][c2] != RED && board[r2][c2] != RED_KING)
				return false;  // There is no red piece to jump.
			return true;  // The jump is legal.
		}

	}  // end canJump()


	private boolean canMove(int player, int r1, int c1, int r2, int c2) {
		// This is called by the getLegalMoves() method to determine whether
		// the player can legally move from (r1,c1) to (r2,c2).  It is
		// assumed that (r1,r2) contains one of the player's pieces and
		// that (r2,c2) is a neighboring square.

		if (r2 < 0 || r2 >= 8 || c2 < 0 || c2 >= 8)
			return false;  // (r2,c2) is off the board.

		if (board[r2][c2] != EMPTY)
			return false;  // (r2,c2) already contains a piece.

		if (player == RED) {
			if (board[r1][c1] == RED && r2 > r1)
				return false;  // Regualr red piece can only move down.
			return true;  // The move is legal.
		}
		else {
			if (board[r1][c1] == BLACK && r2 < r1)
				return false;  // Regular black piece can only move up.
			return true;  // The move is legal.
		}

	}
	
	public CheckersDataBritish Clone(){
		CheckersDataBritish res = new CheckersDataBritish();
		res.setCurrentPlayer(this.currentPlayer);
		res.setGameInProgress(this.gameInProgress);
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				res.board[i][j] = board[i][j];
			}
		}
		res.legalMoves = this.legalMoves.clone();
		
		return res;	
	}
	
} // end class CheckersData