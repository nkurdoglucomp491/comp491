package game;

public class CheckersMoveTurkish extends CheckersMove{
		
	CheckersMoveTurkish(int r1, int c1, int r2, int c2) {
		// Constructor.  Just set the values of the instance variables.
		super(r1, c1, r2, c2, false);
	}
	
	CheckersMoveTurkish(int r1, int c1, int r2, int c2, boolean isJump) {
		// Constructor.  Just set the values of the instance variables.
		super(r1, c1, r2, c2, isJump);
	}
	public boolean isJump() {
		return this.isJump;
	}
	
	public CheckersMoveTurkish clone() {
		return new CheckersMoveTurkish(this.fromRow, this.fromCol, this.toRow, this.toCol, this.isJump);
	}
}  // end class CheckersMove.