package game;

public abstract class CheckersData implements CheckersDataInterface{

	public int selectedRow;
	public int selectedCol;
	int currentPlayer;// Whose turn is it now?  The possible values
	boolean gameInProgress; // Is a game currently in progress?
	
	public int[][] board;  // board[r][c] is the contents of row r, column c.  
	
	public CheckersData() {
		board = new int[8][8];
	}
	
	public int pieceAt(int row, int col) {
		return board[row][col];
	}

	public void setPieceAt(int row, int col, int piece) {
		board[row][col] = piece;
	}
	
	public boolean isGameInProgress() {
		return gameInProgress;
	}

	public void setGameInProgress(boolean gameInProgress) {
		this.gameInProgress = gameInProgress;
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(int currentPlayer) {
		this.currentPlayer = currentPlayer;
	}

	public int[][] getBoard() {
		return board;
	}
}
