package game;

public class CheckersMoveBritish extends CheckersMove{
	// A CheckersMove object represents a move in the game of Checkers.
	// It holds the row and column of the piece that is to be moved
	// and the row and column of the square to which it is to be moved.
	// (This class makes no guarantee that the move is legal.)
	
	CheckersMoveBritish(int r1, int c1, int r2, int c2) {
		// Constructor.  Just set the values of the instance variables.
		super(r1, c1, r2, c2, false);
	}
	public boolean isJump() {
		return (fromRow - toRow == 2 || fromRow - toRow == -2);
	}
	public CheckersMoveBritish clone() {
		return new CheckersMoveBritish(fromRow, fromCol, toRow, toCol);
	}
}  // end class CheckersMove.