package game;

public interface CheckersDataInterface {
	public void setUpGame();
	public void makeMove(CheckersMove move);
	public void makeMove(int fromRow, int fromCol, int toRow, int toCol);
	public CheckersMove[] getLegalMoves(int player);
	public CheckersMove[] getLegalJumpsFrom(int player, int row, int col);
	public CheckersData Clone();
}
