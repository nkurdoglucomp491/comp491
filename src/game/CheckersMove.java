package game;

public class CheckersMove {

	public int fromRow;  // Position of piece to be moved.
	public int fromCol;
	public int toRow;      // Square it is to move to.
	public int toCol;
	
	public final boolean isJump;
	
	public CheckersMove(int r1, int c1, int r2, int c2) {
		fromRow = r1;
		fromCol = c1;
		toRow = r2;
		toCol = c2;
		isJump = false;
	}
	
	CheckersMove(int r1, int c1, int r2, int c2, boolean isJump) {
		// Constructor.  Just set the values of the instance variables.
		fromRow = r1;
		fromCol = c1;
		toRow = r2;
		toCol = c2;
		this.isJump = isJump;
		
	}

}
