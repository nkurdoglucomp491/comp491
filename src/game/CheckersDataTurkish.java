package game;

import java.util.Vector;

public class CheckersDataTurkish extends CheckersData{

	public CheckersMoveTurkish[] legalMoves;
	
	public static final int
	EMPTY = 0,
	RED = -1,
	RED_KING = -4,
	BLACK = 1,
	BLACK_KING = 4;

	public CheckersDataTurkish() {
		// Constructor.  Create the board and set it up for a new game.
		super();
		setUpGame();
	}

	public void setUpGame() {
		for (int row = 1; row < 3; row++) {
			for (int col = 0; col < 8; col++) {
				board[row][col] = BLACK;
			}
		}
		for (int row = 5; row < 7; row++) {
			for (int col = 0; col < 8; col++) {
				board[row][col] = RED;
			}
		}
	}

	public void makeMove(CheckersMove move) {
		makeMove(move.fromRow, move.fromCol, move.toRow, move.toCol);
	}


	public void makeMove(int fromRow, int fromCol, int toRow, int toCol) {
		board[toRow][toCol] = board[fromRow][fromCol];
		board[fromRow][fromCol] = EMPTY;
		
		if (fromCol == toCol) {
			int fromR = fromRow;
			int toR = toRow-1;
			if (fromRow>toRow) {
				fromR = toRow+1;
				toR = fromRow;
			}
			
			for (int row = fromR; row <= toR; row++) {
				if (board[row][fromCol] != EMPTY) {
					board[row][fromCol] = EMPTY;
				}
			}
		} else {
			int fromC = fromCol;
			int toC = toCol-1;
			if (fromCol>toCol) {
				fromC = toCol+1;
				toC = fromCol;
			}
			for (int col = fromC; col <= toC; col++) {
				if (board[fromRow][col] != EMPTY) {
					board[fromRow][col] = EMPTY;
				}
			}
		}
		
		if (toRow == 0 && board[toRow][toCol] == RED)
			board[toRow][toCol] = RED_KING;
		if (toRow == 7 && board[toRow][toCol] == BLACK)
			board[toRow][toCol] = BLACK_KING;
		
	}


	public CheckersMoveTurkish[] getLegalMoves(int player) {
		if (player != RED && player != BLACK)
			return null;

		int playerKing;  // The constant representing a King belonging to player.
		if (player == RED)
			playerKing = RED_KING;
		else
			playerKing = BLACK_KING;

		Vector<CheckersMoveTurkish> moves = new Vector<CheckersMoveTurkish>();  // Moves will be stored in this vector.

		for (int row = 0; row < 8; row++) {
			for (int col = 0; col < 8; col++) {
				if (board[row][col] == player) {
					if (canJump(player, row, col, row+1, col, row+2, col))
						moves.addElement(new CheckersMoveTurkish( row, col, row+2, col, true));
					if (canJump(player, row, col, row-1, col, row-2, col))
						moves.addElement(new CheckersMoveTurkish( row, col, row-2, col, true));
					if (canJump(player, row, col, row, col+1, row, col+2))
						moves.addElement(new CheckersMoveTurkish( row, col, row, col+2, true));
					if (canJump(player, row, col, row, col-1, row, col-2))
						moves.addElement(new CheckersMoveTurkish(row, col, row, col-2, true));
				}else if (board[row][col] == playerKing) {
					for (int row2 = 0; row2 < 8; row2++) {
						if (canJumpKing(player, row, col, row2, col))
							moves.addElement(new CheckersMoveTurkish( row, col, row2, col, true));
					}
					for (int col2 = 0; col2 < 8; col2++) {
						if (canJumpKing(player, row, col, row, col2))
							moves.addElement(new CheckersMoveTurkish( row, col, row, col2, true));
					}
				}

			}

		}

		if (moves.size() == 0) {
			for (int row = 0; row < 8; row++) {
				for (int col = 0; col < 8; col++) {
					if (board[row][col] == player) {
						if (canMove(player,row,col,row+1,col))
							moves.addElement(new CheckersMoveTurkish(row,col,row+1,col, false));
						if (canMove(player,row,col,row-1,col))
							moves.addElement(new CheckersMoveTurkish(row,col,row-1,col, false));
						if (canMove(player,row,col,row,col+1))
							moves.addElement(new CheckersMoveTurkish(row,col,row,col+1, false));
						if (canMove(player,row,col,row,col-1))
							moves.addElement(new CheckersMoveTurkish(row,col,row,col-1, false));
					}
					if (board[row][col] == playerKing) {
						for (int row2 = 0; row2 < 8; row2++) {
							if (canMoveKing(player,row,col,row2,col))
								moves.addElement(new CheckersMoveTurkish(row,col,row2,col, false));
						}
						for (int col2 = 0; col2 < 8; col2++) {
							if (canMoveKing(player,row,col,row,col2))
								moves.addElement(new CheckersMoveTurkish(row,col,row,col2, false));
						}

					}
				}
			}
		}
		if (moves.size() == 0)
			return null;
		else {
			CheckersMoveTurkish[] moveArray = new CheckersMoveTurkish[moves.size()];
			for (int i = 0; i < moves.size(); i++)
				moveArray[i] = moves.elementAt(i);
			return moveArray;
		}

	}


	public CheckersMoveTurkish[] getLegalJumpsFrom(int player, int row, int col) {

		if (player != RED && player != BLACK)
			return null;
		int playerKing;  // The constant representing a King belonging to player.
		if (player == RED)
			playerKing = RED_KING;
		else
			playerKing = BLACK_KING;

		Vector<CheckersMoveTurkish> moves = new Vector<CheckersMoveTurkish>();
		if (board[row][col] == player) {
			if (canJump(player, row, col, row+1, col, row+2, col))
				moves.addElement(new CheckersMoveTurkish( row, col, row+2, col, true));
			if (canJump(player, row, col, row-1, col, row-2, col))
				moves.addElement(new CheckersMoveTurkish( row, col, row-2, col, true));
			if (canJump(player, row, col, row, col+1, row, col+2))
				moves.addElement(new CheckersMoveTurkish( row, col, row, col+2, true));
			if (canJump(player, row, col, row, col-1, row, col-2))
				moves.addElement(new CheckersMoveTurkish(row, col, row, col-2, true));
		}else if (board[row][col] == playerKing) {
			for (int row2 = 0; row2 < 8; row2++) {
				if (canJumpKing(player, row, col, row2, col))
					moves.addElement(new CheckersMoveTurkish( row, col, row2, col, true));
			}
			for (int col2 = 0; col2 < 8; col2++) {
				if (canJumpKing(player, row, col, row, col2))
					moves.addElement(new CheckersMoveTurkish( row, col, row, col2, true));
			}
		}
		if (moves.size() == 0)
			return null;
		else {
			CheckersMoveTurkish[] moveArray = new CheckersMoveTurkish[moves.size()];
			for (int i = 0; i < moves.size(); i++)
				moveArray[i] = moves.elementAt(i);
			return moveArray;
		}
	}  // end getLegalMovesFrom()

	private boolean canJumpKing(int player, int r1, int c1, int r2, int c2){
		if (r2 < 0 || r2 >= 8 || c2 < 0 || c2 >= 8)
			return false;  // (r2,c2) is off the board.
		
		if (board[r2][c2] != EMPTY) {
			return false;
		}
		
		int countRed = 0;
		int countBlack = 0;
		
		if (c1==c2) {
			int fromR = r1+1;
			int toR = r2;
			if (r1>r2) {
				fromR = r2;
				toR = r1-1;
			}
			
			for (int row = fromR; row <= toR; row++) {
				if (board[row][c1] == RED || board[row][c1] == RED_KING) {
					countRed++;
				}else if (board[row][c1] == BLACK || board[row][c1] == BLACK_KING) {
					countBlack++;
				}
				
			}
		} else {
			int fromC = c1+1;
			int toC = c2;
			if (c1>c2) {
				fromC = c2;
				toC = c1-1;
			}
			for (int col = fromC; col <= toC; col++) {
				if (board[r1][col] == BLACK || board[r1][col] == BLACK_KING) {
					countBlack++;
				}else if (board[r1][col] == RED || board[r1][col] == RED_KING) {
					countRed++;
				}
			}
		}
		if (currentPlayer == RED && countRed == 0 && countBlack == 1) {
			return true;
		}else if (currentPlayer == BLACK && countRed == 1 && countBlack == 0){
			return true;
		}
		return false;
	}

	private boolean canJump(int player, int r1, int c1, int r2, int c2, int r3, int c3) {
		if (r3 < 0 || r3 >= 8 || c3 < 0 || c3 >= 8)
			return false;  // (r3,c3) is off the board.

		if (board[r3][c3] != EMPTY)
			return false;  // (r3,c3) already contains a piece.

		if (player == RED) {
			if (board[r1][c1] == RED && r3 > r1)
				return false;  // Regular red piece can only move  up.
			if (board[r2][c2] != BLACK && board[r2][c2] != BLACK_KING)
				return false;  // There is no black piece to jump.
			return true;  // The jump is legal.
		}
		else {
			if (board[r1][c1] == BLACK && r3 < r1)
				return false;  // Regular black piece can only move downn.
			if (board[r2][c2] != RED && board[r2][c2] != RED_KING)
				return false;  // There is no red piece to jump.
			return true;  // The jump is legal.
		}

	}  // end canJump()
	private boolean canMoveKing(int player, int r1, int c1, int r2, int c2) {
		if (r2 < 0 || r2 >= 8 || c2 < 0 || c2 >= 8)
			return false;  // (r2,c2) is off the board.
		
		if (c1==c2) {
			int fromR = r1+1;
			int toR = r2;
			if (r1>r2) {
				fromR = r2;
				toR = r1-1;
			}
			
			for (int row = fromR; row <= toR; row++) {
				if (board[row][c1] != EMPTY) {
					return false;
				}
			}
		} else {
			int fromC = c1+1;
			int toC = c2;
			if (c1>c2) {
				fromC = c2;
				toC = c1-1;
			}
			for (int col = fromC; col <= toC; col++) {
				if (board[r1][col] != EMPTY) {
					return false;
				}
			}
		}
		
		return true;
	}

	private boolean canMove(int player, int r1, int c1, int r2, int c2) {

		if (r2 < 0 || r2 >= 8 || c2 < 0 || c2 >= 8)
			return false;  // (r2,c2) is off the board.

		if (board[r2][c2] != EMPTY)
			return false;  // (r2,c2) already contains a piece.

		if (player == RED) {
			if (board[r1][c1] == RED && r2 > r1)
				return false;  // Regualr red piece can only move down.
			return true;  // The move is legal.
		}
		else {
			if (board[r1][c1] == BLACK && r2 < r1)
				return false;  // Regular black piece can only move up.
			return true;  // The move is legal.
		}

	}  // end canMove()

	public CheckersDataTurkish Clone(){
		CheckersDataTurkish res = new CheckersDataTurkish();
		res.setCurrentPlayer(this.currentPlayer);
		res.setGameInProgress(this.gameInProgress);
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				res.board[i][j] = board[i][j];
			}
		}
		res.legalMoves = this.legalMoves.clone();
		
		return res;	
	}

}