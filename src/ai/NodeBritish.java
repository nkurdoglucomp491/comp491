package ai;

import game.CheckersDataBritish;
import game.CheckersMoveBritish;
import java.util.ArrayList;
import java.util.Random;

class NodeBritish {

	private final CheckersDataBritish state;
	private ArrayList<NodeBritish> childs = new ArrayList<NodeBritish>();
	private final CheckersMoveBritish[] moves;

	private final int boardSum;
	private int gameValue;

	protected final CheckersMoveBritish parentMove;
	
	protected NodeBritish(CheckersDataBritish cd) {
		this.state = cd;
		this.moves = this.state.getLegalMoves(this.state.getCurrentPlayer());

		int sum = 0;
		for (int i = 0; i < this.state.getBoard().length; i++) {
			for (int j = 0; j < this.state.getBoard()[0].length; j++) {
				sum += this.state.getBoard()[i][j];
			}
		}
		this.boardSum = sum;
		parentMove = null;		
	}

	protected NodeBritish(CheckersDataBritish cd, CheckersMoveBritish parentMove) {
		this.state = cd;
		this.moves = this.state.legalMoves;

		int sum = 0;
		for (int i = 0; i < this.state.getBoard().length; i++) {
			for (int j = 0; j < this.state.getBoard()[0].length; j++) {
				sum += this.state.getBoard()[i][j];
			}
		}
		this.boardSum = sum;
		this.parentMove = parentMove.clone();
	}

	protected void growChilds(){
		if (this.state.isGameInProgress() && this.moves != null) {

			for (int i = 0; i < this.moves.length; i++) {
				CheckersDataBritish temp = state.Clone();
				doMakeMove(temp, this.moves[i].clone());
				childs.add(new NodeBritish(temp, this.moves[i].clone()));
			}
		}
	}

	private void utilityFunc(){
		if (this.childs.size() == 0) {
			if (this.state.isGameInProgress()) {
				this.gameValue = this.boardSum;
			}else {
				this.gameValue = this.boardSum*100;
			}
		} else {
			this.gameValue = bestChildValue();
		}
	}

	protected CheckersMoveBritish bestMove(){
		utilityFunc();

		int best = -100;
		CheckersMoveBritish res = null;
		for (int i = 0; i < childs.size(); i++) {
			if (childs.get(i).getGameValue() > best) {
				best = childs.get(i).getGameValue();
				res = childs.get(i).parentMove;
			}else if (childs.get(i).getGameValue() == best) {
				Random r = new Random();
				if (r.nextBoolean()) {
					res = childs.get(i).parentMove;
				}
			}
		}
		return res;
	}

	private int bestChildValue() {
		if (this.state.getCurrentPlayer() == CheckersDataBritish.BLACK) {
			int res = -100;
			for (int i = 0; i < this.childs.size(); i++) {
				this.childs.get(i).utilityFunc();

				if (this.childs.get(i).getGameValue() > res) {
					res = this.childs.get(i).getGameValue();
				}
			}
			return res;
		} else {
			int res = 100;
			for (int i = 0; i < this.childs.size(); i++) {
				this.childs.get(i).utilityFunc();

				if (this.childs.get(i).getGameValue() < res) {
					res = this.childs.get(i).getGameValue();
				}
			}
			return res;
		}
	}

	protected int getGameValue() {
		return gameValue;
	}

	protected ArrayList<NodeBritish> getChilds() {
		return childs;
	}

	protected void doMakeMove(CheckersDataBritish game, CheckersMoveBritish move) {

		game.makeMove(move);

		if (move.isJump()) {
			game.legalMoves = game.getLegalJumpsFrom(game.getCurrentPlayer(),move.toRow,move.toCol);
			if (game.legalMoves != null) {

				game.selectedRow = move.toRow; 
				game.selectedCol = move.toCol;
				return;
			}
		}

		if (game.getCurrentPlayer() == CheckersDataBritish.RED) {
			game.setCurrentPlayer(CheckersDataBritish.BLACK);
			game.legalMoves = game.getLegalMoves(CheckersDataBritish.BLACK);

		}
		else {
			game.setCurrentPlayer(CheckersDataBritish.RED);
			game.legalMoves = game.getLegalMoves(game.getCurrentPlayer());

			game.selectedRow = -1;

			if (game.legalMoves != null) {
				boolean sameStartSquare = true;
				for (int i = 1; i < game.legalMoves.length; i++)
					if (game.legalMoves[i].fromRow != game.legalMoves[0].fromRow
					|| game.legalMoves[i].fromCol != game.legalMoves[0].fromCol) {
						sameStartSquare = false;
						break;
					}
				if (sameStartSquare) {
					game.selectedRow = game.legalMoves[0].fromRow;
					game.selectedCol = game.legalMoves[0].fromCol;
				}
			}

		}
	}
}