package ai;

import game.*;

public class ArtificialPlayerBritish{

	private NodeBritish root;

	public ArtificialPlayerBritish(CheckersDataBritish cd, int level) {
		this.root = new NodeBritish(cd);
		makeTree(level);
	}

	//creates game tree with depth
	public void makeTree(int depth){
		makeTreeHelper(depth, root);
	}

	private void makeTreeHelper(int depth, NodeBritish n){
		if (depth > 0) {
			n.growChilds();
			depth--;
			
			for (int i = 0; i < n.getChilds().size(); i++) {
				makeTreeHelper(depth, n.getChilds().get(i));
			}
		}
	}

	public CheckersMoveBritish getBestMove(){
		CheckersMoveBritish res = this.root.bestMove();
		
		for (int i = 0; i < root.getChilds().size(); i++) {
			if (root.getChilds().get(i).parentMove.equals(res)) {
				this.root = root.getChilds().get(i);
				growLeaves(this.root);
				break;
			}
		}
		return res;
	}
	
	public void applyOpponentMove(CheckersMoveBritish opponentMove){
		for (int i = 0; i < root.getChilds().size(); i++) {
			CheckersMove temp = root.getChilds().get(i).parentMove;
			if (temp.fromCol == opponentMove.fromCol &&
					temp.fromRow == opponentMove.fromRow &&
					temp.toCol == opponentMove.toCol &&
					temp.toRow == opponentMove.toRow) {
				this.root = root.getChilds().get(i);
				growLeaves(root);
				break;
			}
		}
	}

	private void growLeaves(NodeBritish n) {
		if (n.getChilds().size() == 0) {
			n.growChilds();
		}else {
			for (int i = 0; i < n.getChilds().size(); i++) {
				growLeaves(n.getChilds().get(i));
			}
		}
	}

}
