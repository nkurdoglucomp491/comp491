package ai;

import game.CheckersDataTurkish;
import game.CheckersMoveTurkish;
public class ArtificialPlayerTurkish {

	private NodeTurkish root;

	public ArtificialPlayerTurkish(CheckersDataTurkish cd, int level) {
		this.root = new NodeTurkish(cd);
		makeTree(level);
	}

	//creates game tree with depth
	public void makeTree(int depth){
		makeTreeHelper(depth, root);
	}

	private void makeTreeHelper(int depth, NodeTurkish n){
		if (depth > 0) {
			n.growChilds();
			depth--;
			
			for (int i = 0; i < n.getChilds().size(); i++) {
				makeTreeHelper(depth, n.getChilds().get(i));
			}
		}
	}

	public CheckersMoveTurkish getBestMove(){
		CheckersMoveTurkish res = this.root.bestMove();
		
		for (int i = 0; i < root.getChilds().size(); i++) {
			if (root.getChilds().get(i).parentMove.equals(res)) {
				this.root = root.getChilds().get(i);
				growLeaves(this.root);
				break;
			}
		}
		return res;
	}
	
	public void applyOpponentMove(CheckersMoveTurkish opponentMove){
		for (int i = 0; i < root.getChilds().size(); i++) {
			CheckersMoveTurkish temp = root.getChilds().get(i).parentMove;
			if (temp.fromCol == opponentMove.fromCol &&
					temp.fromRow == opponentMove.fromRow &&
					temp.toCol == opponentMove.toCol &&
					temp.toRow == opponentMove.toRow) {
				this.root = root.getChilds().get(i);
				growLeaves(root);
				break;
			}
		}
	}

	private void growLeaves(NodeTurkish n) {
		if (n.getChilds().size() == 0) {
			n.growChilds();
		}else {
			for (int i = 0; i < n.getChilds().size(); i++) {
				growLeaves(n.getChilds().get(i));
			}
		}
	}

}
