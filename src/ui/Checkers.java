package ui;
import java.awt.*;
import java.applet.*;
import javax.swing.JOptionPane;

public class Checkers extends Applet {

	public void init() {
		
		setLayout(null);
		setBackground(new Color(0,150,0)); 
		Object[] possibilities = {"British","Turkish"};
		String level = (String)JOptionPane.showInputDialog(null, "Select Checkers Gme: ", "Game Type",
				JOptionPane.PLAIN_MESSAGE, null, possibilities, possibilities[1]);

		CheckersCanvas board;
		if (level.equals("British")) {
			board = new CheckersCanvasBritish();
		} else{
			board = new CheckersCanvasTurkish();
		}
		add(board);

		board.newGameButton.setBackground(Color.lightGray);
		add(board.newGameButton);

		board.resignButton.setBackground(Color.lightGray);
		add(board.resignButton);
		
		add(board.editBox);

		board.message.setForeground(Color.green);
		board.message.setFont(new Font("Serif", Font.BOLD, 14));
		add(board.message);
		
		board.setBounds(20,20,164*CheckersCanvasBritish.canvasRatio,164*CheckersCanvasBritish.canvasRatio); // Note:  size MUST be 164-by-164 !
		board.newGameButton.setBounds(180*CheckersCanvasBritish.canvasRatio, 20*CheckersCanvasBritish.canvasRatio, 100, 30);
		board.resignButton.setBounds(180*CheckersCanvasBritish.canvasRatio, 40*CheckersCanvasBritish.canvasRatio, 100, 30);
		board.editBox.setBounds(180*CheckersCanvasBritish.canvasRatio, 60*CheckersCanvasBritish.canvasRatio, 100, 30);
		board.message.setBounds(100, 170*CheckersCanvasBritish.canvasRatio, 330, 30);
	}

}