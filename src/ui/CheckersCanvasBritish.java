package ui;
import game.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import ai.ArtificialPlayerBritish;

class CheckersCanvasBritish extends CheckersCanvas implements ActionListener, MouseListener {

	CheckersDataBritish game;  // The data for the checkers board is kept here.
	ArtificialPlayerBritish AI;
	
	public CheckersCanvasBritish(){

		setBackground(Color.black);
		addMouseListener(this);
		setFont(new  Font("Serif", Font.BOLD, 14));
		editBox = new JCheckBox("Edit Deck");
		editBox.addActionListener(this);
		resignButton = new Button("Resign");
		resignButton.addActionListener(this);
		newGameButton = new Button("New Game");
		newGameButton.addActionListener(this);
		message = new Label("",Label.CENTER);
		game = new CheckersDataBritish();

		doNewGame();
	}


	public void actionPerformed(ActionEvent evt) {
		Object src = evt.getSource();
		if (src == newGameButton)
			doNewGame();
		else if (src == resignButton)
			doResign();
	}


	void doNewGame() {
		Object[] possibilities = {"easy","medium","hard","expert"};
		String level = (String)JOptionPane.showInputDialog(null, "Select level: ", "Game Level",
				JOptionPane.PLAIN_MESSAGE, null, possibilities, possibilities[1]);

		if (level.equals("easy")) {
			calculateSteps = 4;
		} else if(level.equals("medium")){
			calculateSteps = 5;
		}else if (level.equals("hard")) {
			calculateSteps = 6;
		}else if (level.equals("expert")) {
			calculateSteps = 7;
		}
		if (game.isGameInProgress() == true) {

			message.setText("Finish the current game first!");
			return;
		}
		game.setUpGame();   // Set up the pieces.
		this.game.setCurrentPlayer(CheckersDataBritish.RED);
		game.legalMoves = game.getLegalMoves(CheckersDataBritish.RED);  // Get RED's legal moves.
		game.selectedRow = -1;   // RED has not yet selected a piece to move.
		message.setText("Red:  Make your move.");
		game.setGameInProgress(true);
		newGameButton.setEnabled(false);
		resignButton.setEnabled(true);

		AI = new ArtificialPlayerBritish(game.Clone(), calculateSteps);
		repaint();
	}


	void doResign() {
		if (game.isGameInProgress() == false) {
			message.setText("There is no game in progress!");
			return;
		}
		if (game.getCurrentPlayer() == CheckersDataBritish.RED)
			gameOver("RED resigns.  BLACK wins.");
		else
			gameOver("BLACK resigns.  RED winds.");
	}


	void gameOver(String str) {

		message.setText(str);
		newGameButton.setEnabled(true);
		resignButton.setEnabled(false);
		game.setGameInProgress(false);
	}


	void doClickSquare(int row, int col) {
		message.setText("Click the square you want to change.");
		game.legalMoves = new CheckersMoveBritish[0];
		if (editBox.isSelected()) {
			int temp = (row + col)%2;
			if (temp == 0) {
				changed = true;
				game.selectedRow = row;
				game.selectedCol = col;
				if (game.board[game.selectedRow][game.selectedCol] == game.EMPTY) {
					game.board[game.selectedRow][game.selectedCol] = game.RED;
				} else if(game.board[game.selectedRow][game.selectedCol] == game.RED){
					game.board[game.selectedRow][game.selectedCol] = game.RED_KING;
				}else if (game.board[game.selectedRow][game.selectedCol] == game.RED_KING) {
					game.board[game.selectedRow][game.selectedCol] = game.BLACK;
				}else if (game.board[game.selectedRow][game.selectedCol] == game.BLACK) {
					game.board[game.selectedRow][game.selectedCol] = game.BLACK_KING;
				}else if (game.board[game.selectedRow][game.selectedCol] == game.BLACK_KING){
					game.board[game.selectedRow][game.selectedCol] = game.EMPTY;
				}
				repaint();
			}
		}else if(changed){
			changed = false;
			AI = new ArtificialPlayerBritish(game.Clone(), calculateSteps);
		}else{
			game.legalMoves = game.getLegalMoves(game.getCurrentPlayer());
			for (int i = 0; i < game.legalMoves.length; i++)
				if (game.legalMoves[i].fromRow == row && game.legalMoves[i].fromCol == col) {
					game.selectedRow = row;
					game.selectedCol = col;
					if (game.getCurrentPlayer() == CheckersDataBritish.RED)
						message.setText("RED:  Make your move.");
					else
						message.setText("BLACK:  Make your move.");
					repaint();
					return;
				}

			if (game.selectedRow < 0) {
				message.setText("Click the piece you want to move.");
				return;
			}

			for (int i = 0; i < game.legalMoves.length; i++)
				if (game.legalMoves[i].fromRow == game.selectedRow && game.legalMoves[i].fromCol == game.selectedCol
				&& game.legalMoves[i].toRow == row && game.legalMoves[i].toCol == col) {

					CheckersMoveBritish temp = game.legalMoves[i].clone();
					doMakeMove(game.legalMoves[i]);
					paint(this.getGraphics());
					
					AI.applyOpponentMove(temp);
					while (game.getCurrentPlayer() == CheckersDataBritish.BLACK) {
						
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						paint(this.getGraphics());
						doMakeMove(AI.getBestMove());
					}

					return;
				}

			message.setText("Click the square you want to move to.");
		}

	}

	void doMakeMove(CheckersMoveBritish move) {

		game.makeMove(move);

		if (move.isJump()) {
			game.legalMoves = game.getLegalJumpsFrom(game.getCurrentPlayer(),move.toRow,move.toCol);
			if (game.legalMoves != null) {
				if (game.getCurrentPlayer() == CheckersDataBritish.RED)
					message.setText("RED:  You must continue jumping.");
				else
					message.setText("BLACK:  You must continue jumping.");
				game.selectedRow = move.toRow;
				game.selectedCol = move.toCol;
				repaint();
				return;
			}
		}

		if (game.getCurrentPlayer() == CheckersDataBritish.RED) {
			game.setCurrentPlayer(CheckersDataBritish.BLACK);
			game.legalMoves = game.getLegalMoves(CheckersDataBritish.BLACK);
			if (game.legalMoves == null)
				gameOver("BLACK has no moves.  RED wins.");
			else if (game.legalMoves[0].isJump())
				message.setText("BLACK:  Make your move.  You must jump.");
			else
				message.setText("BLACK:  Make your move.");
		}
		else {
			game.setCurrentPlayer(CheckersDataBritish.RED);
			game.legalMoves = game.getLegalMoves(game.getCurrentPlayer());
			if (game.legalMoves == null)
				gameOver("RED has no moves.  BLACK wins.");
			else if (game.legalMoves[0].isJump())
				message.setText("RED:  Make your move.  You must jump.");
			else
				message.setText("RED:  Make your move.");
		}

		game.selectedRow = -1;

		if (game.legalMoves != null) {
			boolean sameStartSquare = true;
			for (int i = 1; i < game.legalMoves.length; i++)
				if (game.legalMoves[i].fromRow != game.legalMoves[0].fromRow
				|| game.legalMoves[i].fromCol != game.legalMoves[0].fromCol) {
					sameStartSquare = false;
					break;
				}
			if (sameStartSquare) {
				game.selectedRow = game.legalMoves[0].fromRow;
				game.selectedCol = game.legalMoves[0].fromCol;
			}
		}
		repaint();

	}


	public void update(Graphics g) {
		paint(g);
	}


	public void paint(Graphics g) {
		g.setColor(Color.black);
		g.drawRect(0,0,getSize().width*canvasRatio-1*canvasRatio,getSize().height*canvasRatio-1*canvasRatio);
		g.drawRect(1*canvasRatio,1*canvasRatio,getSize().width*canvasRatio-3*canvasRatio,getSize().height*canvasRatio-3*canvasRatio);

		for (int row = 0; row < 8; row++) {
			for (int col = 0; col < 8; col++) {
				if ( row % 2 == col % 2 )
					g.setColor(Color.lightGray);
				else
					g.setColor(Color.gray);
				g.fillRect(2*canvasRatio + col*20*canvasRatio, 2*canvasRatio + row*20*canvasRatio, 20*canvasRatio, 20*canvasRatio);
				switch (game.pieceAt(row,col)) {
				case CheckersDataBritish.RED:
					g.setColor(Color.red);
					g.fillOval(4*canvasRatio + col*20*canvasRatio, 4*canvasRatio + row*20*canvasRatio, 16*canvasRatio, 16*canvasRatio);
					break;
				case CheckersDataBritish.BLACK:
					g.setColor(Color.black);
					g.fillOval(4*canvasRatio + col*20*canvasRatio, 4*canvasRatio + row*20*canvasRatio, 16*canvasRatio, 16*canvasRatio);
					break;
				case CheckersDataBritish.RED_KING:
					g.setColor(Color.red);
					g.fillOval(4*canvasRatio + col*20*canvasRatio, 4*canvasRatio + row*20*canvasRatio, 16*canvasRatio, 16*canvasRatio);
					g.setColor(Color.white);
					g.drawString("K", 7*canvasRatio + col*20*canvasRatio, 16*canvasRatio + row*20*canvasRatio);
					break;
				case CheckersDataBritish.BLACK_KING:
					g.setColor(Color.black);
					g.fillOval(4*canvasRatio + col*20*canvasRatio, 4*canvasRatio + row*20*canvasRatio, 16*canvasRatio, 16*canvasRatio);
					g.setColor(Color.white);
					g.drawString("K", 7*canvasRatio + col*20*canvasRatio, 16*canvasRatio + row*20*canvasRatio);
					break;
				}
			}
		}

		if (game.isGameInProgress()) {
			g.setColor(Color.cyan);
			for (int i = 0; i < game.legalMoves.length; i++) {
				g.drawRect(2*canvasRatio + game.legalMoves[i].fromCol*20*canvasRatio, 2*canvasRatio + game.legalMoves[i].fromRow*20*canvasRatio, 19*canvasRatio, 19*canvasRatio);
			}

			if (game.selectedRow >= 0) {
				g.setColor(Color.white);
				g.drawRect(2*canvasRatio + game.selectedCol*20*canvasRatio, 2*canvasRatio + game.selectedRow*20*canvasRatio, 19*canvasRatio, 19*canvasRatio);
				g.drawRect(3*canvasRatio + game.selectedCol*20*canvasRatio, 3*canvasRatio + game.selectedRow*20*canvasRatio, 17*canvasRatio, 17*canvasRatio);
				g.setColor(Color.green);
				for (int i = 0; i < game.legalMoves.length; i++) {
					if (game.legalMoves[i].fromCol == game.selectedCol && game.legalMoves[i].fromRow == game.selectedRow)
						g.drawRect(2*canvasRatio + game.legalMoves[i].toCol*20*canvasRatio, 2*canvasRatio + game.legalMoves[i].toRow*20*canvasRatio, 19*canvasRatio, 19*canvasRatio);
				}
			}
		}
	}


	public Dimension getPreferredSize() {
		return new Dimension(164*canvasRatio, 164*canvasRatio);
	}


	public Dimension getMinimumSize() {
		return new Dimension(164*canvasRatio, 164*canvasRatio);
	}

	////////
	public void mousePressed(MouseEvent evt) {
		if (game.isGameInProgress() == false)
			message.setText("Click \"New Game\" to start a new game.");
		else {
			int col = (evt.getX() - 2) / (20*canvasRatio);
			int row = (evt.getY() - 2) / (20*canvasRatio);
			if (col >= 0 && col < 8 && row >= 0 && row < 8)
				doClickSquare(row,col);
		}
	}


	public void mouseReleased(MouseEvent evt) { }
	public void mouseClicked(MouseEvent evt) { }
	public void mouseEntered(MouseEvent evt) { }
	public void mouseExited(MouseEvent evt) { }


}  // end class SimpleCheckerboardCanvas